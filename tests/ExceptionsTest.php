<?php
namespace Skipper\Exceptions\Tests;

use PHPUnit\Framework\TestCase;
use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;

class ExceptionsTest extends TestCase
{
    public function testMessage()
    {
        $e = new DomainException('test message', 'test');

        $this->assertEquals('test message', $e->getMessage());
        $this->assertCount(1, $e->getErrors());
        $this->assertCount(0, $e->getData());
        $this->assertEquals(0, $e->getCode());
    }

    public function testCode()
    {
        $e = new DomainException('', '', [], null, 123);

        $this->assertEquals(123, $e->getCode());
    }

    public function testData()
    {
        $e = new DomainException('test message', 'test', [
            'msg' => 'test context msg',
        ]);

        $this->assertArrayHasKey('msg', $e->getData());
        $this->assertEquals('test context msg', $e->getData()['msg']);
    }

    public function testErrors()
    {
        $e = new Error('test msg', 'test_reason', 'location.test');

        $this->assertEquals('test msg', $e->getMsg());
        $this->assertEquals('test_reason', $e->getReason());
        $this->assertEquals('location.test', $e->getLocation());
    }

    public function testHasErrors()
    {
        $e = new DomainException('test message', 'test');

        $errors = $e->getErrors();
        $error = reset($errors);

        $this->assertEquals('test message', $error->getMsg());
        $this->assertEquals('test', $error->getLocation());
        $this->assertEquals('internalError', $error->getReason());
    }

    /**
     * @throws DomainException
     */
    public function testException()
    {
        $this->expectExceptionMessage('test message');
        $this->expectExceptionCode(500);
        $this->expectException(DomainException::class);

        throw new DomainException('test message', '', [], null, 500);
    }
}