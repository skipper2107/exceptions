<?php
namespace Skipper\Exceptions;

interface ReasonAwareInterface
{
    /**
     * @return string
     */
    public function getReason(): string;

    /**
     * @param string $reason
     */
    public function setReason(string $reason): void;
}