<?php
namespace Skipper\Exceptions;

use Throwable;

class DomainException extends \Exception implements ErrorAwareInterface
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var Error[]
     */
    protected $errors = [];

    public function __construct(string $message, string $location, array $context = [], Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);

        $this->data = $context;

        $this->addError(new Error($message, 'internalError', $location));
    }

    /**
     * @param Error $error
     */
    public function addError(Error $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @return array
     */
    public function render(): array
    {
        $pretty = [
            'message' => $this->getMessage(),
            'code' => $this->getCode(),
        ];
        if (false === empty($this->data)) {
            $pretty['context'] = $this->getData();
        }
        if (false === empty($this->errors)) {
            foreach ($this->errors as $error) {
                $pretty['errors'][] = [
                    'message' => $error->getMsg(),
                    'reason' => $error->getReason(),
                    'location' => $error->getLocation(),
                ];
            }
        }

        return $pretty;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return Error[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}