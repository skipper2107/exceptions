<?php
namespace Skipper\Exceptions;

interface ErrorAwareInterface
{
    /**
     * @return Error[]
     */
    public function getErrors(): array;

    /**
     * @param Error $error
     */
    public function addError(Error $error): void;
}