<?php
namespace Skipper\Exceptions;

final class Error implements ReasonAwareInterface
{
    /**
     * @var string
     */
    private $msg;

    /**
     * @var string
     */
    private $reason;

    /**
     * @var string
     */
    private $location;

    public function __construct(string $msg, string $reason, string $location)
    {
        $this->msg = $msg;
        $this->reason = $reason;
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getMsg(): string
    {
        return $this->msg;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }
}